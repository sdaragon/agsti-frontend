module.exports = {
  /* Use the Desy default config */
  presets: [require('desy-frontend/config/tailwind.config.js')],
  /* Change PurgeCSS files to add DESY AND this project's files */
  purge: {
    content: ['./node_modules/desy-frontend/src/**/*.html',
              './node_modules/desy-frontend/src/**/*.njk',
              './src/**/*.html',
              './src/**/*.njk',
              './docs/**/*.html',
              './docs/**/*.njk'
              ],
    options: {
      safelist: [
                  'has-offcanvas-open',
                  'has-dialog',
                  'dialog-backdrop',
                  'dialog-backdrop.active',
                  'focus',
                  'dev'
                  ],
    }
  },
  variants: {
    extend: {
      accessibility: ['group-hover'],
    }
  },
  theme: {
    extend:{
      screens: {
        // '2xl': '1536px',
        // '3xl': '1900px'
      },
      minHeight: {
       '48': '12rem',
       '96': '24rem',
      },
      zIndex: {
       '75': 75,
       '10000': 10000,
      }
    }
  }
}
